import json

from flask import Flask
from pymongo import MongoClient
from bson import json_util

app = Flask(__name__)


@app.route('/')
def hello_world():
    return 'Hello James!'

@app.route('/hiring')
def inventory():
    client = MongoClient('localhost', 27017)
    db = client.test
    cursor = db.hiring.find()
    if cursor.count() > 0:
        return json_util.dumps(cursor)

    return '{}'

if __name__ == '__main__':
    app.run()

